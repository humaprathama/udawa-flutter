import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:udawa_flutter/live_sensing.dart';

class Setting extends StatelessWidget {

  String url;
  var initResult;
  // var flush = false;

  Setting(param) {
    url = param;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ), 
        title: Text('Device Detail') 
      ),
      body: FutureBuilder(
        future: http.post("http://$url/system/query", 
          body: {
            "cmd": "system_query",
            "args": "select*from tbl_config",
            "msgId": "1"
          }
        ).then((e) => jsonDecode(e.body)),
        builder: (context, projectSnap) {
          print("get system config...");
          if(projectSnap.connectionState == ConnectionState.done) {
            var result = projectSnap.data['respon'][0];
            return Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.bookmark, color: Colors.cyan[400]),
                    title: Text('Device Name'),
                    subtitle: Text(result["deviceName"])
                  ),
                  ListTile(
                    leading: Icon(Icons.developer_board, color: Colors.cyan[400]),
                    title: Text('WiFi SSID'),
                    subtitle: Text(result["wifiSsid"])
                  ),
                  ListTile(
                    leading: Icon(Icons.memory, color: Colors.cyan[400]),
                    title: Text('WiFi Password'),
                    subtitle: Text(result["wifiPassword"])
                  ),
                  ListTile(
                    leading: Icon(Icons.timer, color: Colors.cyan[400]),
                    title: Text('Planting Date'),
                    subtitle: Text(result["tanggalTanam"])
                  ),
                  ListTile(
                    leading: Icon(Icons.ac_unit, color: Colors.cyan[400]),
                    title: Text('Device Sound'),
                    subtitle: Text(result["flagAlarm"])
                  ),
                  ListTile(
                    leading: Icon(Icons.ac_unit, color: Colors.cyan[400]),
                    title: Text('Pump fail safe'),
                    subtitle: Text(result["pompaFailSafe"])
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
          
            
            
        },
      )
    );
  }
}