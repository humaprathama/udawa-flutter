import 'dart:async';

import 'package:upnp/upnp.dart';

class Devices {
  StreamController<List<Device>> _devices = StreamController();
  StreamController<bool> loading = StreamController();

  Devices() {
    scan();
  }

  Stream<List<Device>> deviceAvailable() {
    return _devices.stream;
  }

  scan() async {
    loading.add(true);
    print('scanning udawa devices...');
      var disc = new DeviceDiscoverer();
      var data = await disc.getDevices().catchError((err) => print("error upnp: $err"));
      var udawas = data.where((u) => u.manufacturer == 'TI UNDIKNAS').toList();
      _devices.add(udawas);
      print('scan done!');
      loading.add(false);
  }

}

var devicesService = new Devices();