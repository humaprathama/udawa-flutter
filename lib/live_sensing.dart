
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class LiveSensing extends StatefulWidget {
  WebSocketChannel channel;
  bool flush = false;

  LiveSensing(String url) {
    print('create websocket connection to $url');
    channel = IOWebSocketChannel.connect("ws://$url:81");
  }

  @override
  _LiveSensing createState() => _LiveSensing();
}

class _LiveSensing extends State<LiveSensing> {

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            widget.channel.sink.close();
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ), 
        title: Text('Device Live Sensing') 
      ),
      body: StreamBuilder(
        stream: widget.channel.stream,
        builder: (context, projectSnap) {
          
          if(projectSnap.hasData) {
            var data = jsonDecode(projectSnap.data)['respon'];
            var memoryUsage = ((data['heapSize']-data['freeHeap'])/data['heapSize']*100).toStringAsFixed(2);
            return Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.memory, color: Colors.cyan[400]),
                    title: Text('Memory Usage'),
                    subtitle: Text(memoryUsage)
                  ),
                  ListTile(
                    leading: Icon(Icons.ac_unit, color: Colors.cyan[400]),
                    title: Text('Temperature'),
                    subtitle: Text(data['temp'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.settings_backup_restore, color: Colors.cyan[400]),
                    title: Text('Current flow rate'),
                    subtitle: Text(data['flow_a_rate_curr'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.settings_backup_restore, color: Colors.cyan[400]),
                    title: Text('Maximum flow rate'),
                    subtitle: Text(data['flow_a_rate_max'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.settings_backup_restore, color: Colors.cyan[400]),
                    title: Text('Average flow rate'),
                    subtitle: Text(data['flow_a_rate_mean'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.blur_circular, color: Colors.cyan[400]),
                    title: Text('flow volumes'),
                    subtitle: Text(data['flow_a_vol'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.timer, color: Colors.cyan[400]),
                    title: Text('Soil A'),
                    subtitle: Text(data['soil_a'].toString())
                  ),
                  ListTile(
                    leading: Icon(Icons.timer, color: Colors.cyan[400]),
                    title: Text('Soil B'),
                    subtitle: Text(data['soil_b'].toString())
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
          
          
            
            
        },
      )
    );
  }

  @override
  void dispose() {
    print('close websocket connection!');
    widget.channel.sink.close();
    super.dispose();
  }
}