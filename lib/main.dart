// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:udawa_flutter/models/scanner.dart';

import 'device_detail.dart';

enum WhyFarther { harder, smarter, selfStarter, tradingCharter }

void main() {
  runApp(MaterialApp(
    title: "Udawa System",
    home: MyApp(),
    theme: ThemeData(
      brightness: Brightness.dark
    )
  ));
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(
          title: Text('Udawa Devices'),
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: (String result) { 
                if(result == 'scan') {
                  devicesService.scan();
                }
              },
              itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                const PopupMenuItem<String>(
                  value: "setup",
                  child: Text('Setup new device'),
                ),
                const PopupMenuItem<String>(
                  value: 'scan',
                  child: Text('Scan devices'),
                )
              ],
            )
          ],
          bottom: PreferredSize(
                    preferredSize: Size(double.infinity, 1.0),
                    child: ProgressBar(),
                  )
        ),
        body: Devices()
      );
  }

}

class Devices extends StatefulWidget {
  @override
  _DevicesState createState() => _DevicesState();
}

class _DevicesState extends State<Devices> {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: devicesService.deviceAvailable(),
      builder: (context, projectSnap) {
        return ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: projectSnap.hasData? projectSnap.data.length : 0,
          itemBuilder: (context, index) {
            
             return Card(
               child: ListTile(
                 leading: Icon(Icons.developer_board, color: Colors.cyan[400]),
                  title: Text(
                    projectSnap.data[index].friendlyName,
                    style: _biggerFont
                  ),
                  subtitle: Text(projectSnap.data[index].urlBase),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DetailDevice(projectSnap.data[index].presentationUrl)),
                    );
                  },
              )
             );
          }
        );
      },
    );
  }
}

const double _kMyLinearProgressIndicatorHeight = 6.0;

class ProgressBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: devicesService.loading.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return LinearProgressIndicator();
        }
        else {
          return Container();
        }
      }
    );
  }
}