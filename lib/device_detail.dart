import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:udawa_flutter/live_sensing.dart';
import 'package:udawa_flutter/setting.dart';

class DetailDevice extends StatelessWidget {

  String url;
  var initResult;
  // var flush = false;

  DetailDevice(param) {
    url = param;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ), 
        title: Text('Device Detail') 
      ),
      body: FutureBuilder(
        future: http.get("http://$url/system/info").then((e) => jsonDecode(e.body)),
        builder: (context, projectSnap) {
          print("get system info...");
          var deviceID = '';
          var deviceName = '';
          var freeMemory = "0";
          var uptime = "0";
          var temp = "0";
          if(projectSnap.connectionState == ConnectionState.done) {
            deviceID = projectSnap.data['respon']['deviceId'];
            deviceName = projectSnap.data['respon']['deviceName'];
            freeMemory = (projectSnap.data['respon']['freeHeap']/projectSnap.data['respon']['heapSize']*100).toStringAsFixed(2);
            uptime = projectSnap.data['respon']['uptime'].toString();
            temp = projectSnap.data['respon']['temp'].toString();
            var flush = projectSnap.data['respon']['flagFlush'];
            return Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.bookmark, color: Colors.cyan[400]),
                    title: Text('Device ID'),
                    subtitle: Text(deviceID)
                  ),
                  ListTile(
                    leading: Icon(Icons.developer_board, color: Colors.cyan[400]),
                    title: Text('Device Name'),
                    subtitle: Text(deviceName)
                  ),
                  ListTile(
                    leading: Icon(Icons.memory, color: Colors.cyan[400]),
                    title: Text('Free memory'),
                    subtitle: Text("$freeMemory %")
                  ),
                  ListTile(
                    leading: Icon(Icons.timer, color: Colors.cyan[400]),
                    title: Text('Uptime'),
                    subtitle: Text(uptime)
                  ),
                  ListTile(
                    leading: Icon(Icons.ac_unit, color: Colors.cyan[400]),
                    title: Text('Temperature'),
                    subtitle: Text("$temp celcius")
                  ),
                  ListTile(
                    leading: Icon(Icons.pin_drop, color: Colors.cyan[400]),
                    title: Text('Manual Flush'),
                    subtitle: Text("Command devices to flush on or off"),
                    trailing: StatefulBuilder(
                      builder: (ctx, setState) {
                        return Switch(
                          activeColor: Colors.cyan[400],
                        value: flush,
                        onChanged: (e) {
                            setState(() {
                              flush = e;
                            });
                          if(e) {
                            print("Start flush...");
                            http.get("http://$url/system/startflush");
                          } else {
                            print("Stop flush...");
                            http.get("http://$url/system/stopflush");
                            
                          }
                        });
                      },
                    )
                  ),
                  ListTile(
                    leading: Icon(Icons.live_tv, color: Colors.cyan[400]),
                    title: Text('Live Sensing'),
                    subtitle: Text("Show streaming data from device live sensing on flushing"),
                    trailing: StatefulBuilder(
                      builder: (ctx, setState) {
                        if(flush) {
                          return Icon(Icons.navigate_next, color: Colors.cyan[400]);
                        } else {
                          return Icon(Icons.navigate_next);
                        }
                      },
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LiveSensing(url)),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.schedule, color: Colors.cyan[400]),
                    title: Text('Flush Schedule'),
                    subtitle: Text("Setup device flush schedule"),
                    trailing: StatefulBuilder(
                      builder: (ctx, setState) {
                        return Icon(Icons.navigate_next, color: Colors.cyan[400]);
                      },
                    ),
                    onTap: () {
                      print("scheduling...");
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.settings, color: Colors.cyan[400]),
                    title: Text('Setting'),
                    subtitle: Text("Device setting, device name, planting date, etc.."),
                    trailing: StatefulBuilder(
                      builder: (ctx, setState) {
                        return Icon(Icons.navigate_next, color: Colors.cyan[400]);
                      },
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Setting(url)),
                      );
                    },
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
            
        }
      )
    );
  }
}